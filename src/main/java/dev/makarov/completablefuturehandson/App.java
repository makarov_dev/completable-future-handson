package dev.makarov.completablefuturehandson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class App {

    public static void main(final String[] args) throws Exception {
        //Уникальный список ключей
        final Set<Integer> keys = Set.of(1, 2, 3, 4, 5);
        //Создаем Future, которая будет содержать посчитанный многопоточно результат
        final Future<Map<Integer, String>> future = CompletableFuture.supplyAsync(new LongTaskSupplier(keys));

        System.out.println("main thread continue");

        final Map<Integer, String> result = future.get();
        result.forEach((k, v) -> System.out.printf("%s = %s\n", k, v));
    }

    /**
     * Основной Supplier для заполнения нашей мапы в многопоточном режиме с ожиданием (10 сек)
     */
    private static class LongTaskSupplier implements Supplier<Map<Integer, String>> {

        private final Set<Integer> keys;

        /**
         * @param keys список ключей, которые нужно обработать
         */
        public LongTaskSupplier(final Set<Integer> keys) {
            this.keys = keys;
        }

        @Override
        public Map<Integer, String> get() {
            final Map<Integer, String> result = new HashMap<>(keys.size());
            final ExecutorService pool = Executors.newFixedThreadPool(4);

            try {
                final List<Future<Tuple<Integer, String>>> futures = pool
                        .invokeAll(keys.stream().map(Task::new).collect(Collectors.toList()));
                pool.awaitTermination(10, TimeUnit.SECONDS);
                futures.forEach(f -> {
                    try {
                        final Tuple<Integer, String> tuple = f.get();
                        result.put(tuple.x, tuple.y);
                    } catch (final InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                });

            } catch (final InterruptedException e) {
                e.printStackTrace();
            }

            return result;
        }
    }

    /**
     * Непосредственно задача вычисления значения мапы
     */
    private static class Task implements Callable<Tuple<Integer, String>> {

        private final Integer key;

        public Task(final Integer key) {
            this.key = key;
        }

        @Override
        public Tuple<Integer, String> call() throws Exception {
            Thread.sleep(1000);
            return new Tuple<>(key, UUID.randomUUID().toString());
        }
    }

    /**
     * Простая реализация Pair
     */
    private static class Tuple<X, Y> {
        public final X x;
        public final Y y;

        public Tuple(final X x, final Y y) {
            this.x = x;
            this.y = y;
        }
    }
}
